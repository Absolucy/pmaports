# Maintainer: Martijn Braam <martijn@brixit.nl>
# Co-Maintainer: Luca Weiss <luca@z3ntu.xyz>
# Co-Maintainer: Bart Ribbers <bribbers@disroot.org>
# Co-Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
_flavor=postmarketos-allwinner
_config="config-$_flavor.$CARCH"
pkgname=linux-$_flavor
pkgver=5.13.1_git20210707
pkgrel=1
_tag="orange-pi-5.13-20210707-1456"
pkgdesc="Kernel fork with Pine64 patches (megi's tree, slightly patched)"
arch="aarch64 armv7"
url="https://megous.com/git/linux/"
license="GPL-2.0-only"
makedepends="
	bison
	devicepkg-dev
	findutils
	flex
	gmp-dev
	installkernel
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	rsync
	xz
	"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox
		 pmb:kconfigcheck-nftables"
source="$pkgname-$_tag.tar.gz::https://github.com/megous/linux/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
	0001-dts-add-dontbeevil-pinephone-devkit.patch
	0002-dts-add-pinetab-dev-old-display-panel.patch
	0003-dts-pinetab-add-missing-bma223-ohci1.patch
	0004-arm64-dts-allwinner-Add-bluetooth-node-to-the-PineTa.patch
	0005-dts-pinetab-make-audio-routing-consistent-with-pinep.patch
	0006-dts-pinephone-remove-bt-firmware-suffix.patch
	0007-media-ov5640-Implement-autofocus.patch
	0008-dts-pinephone-drop-modem-power-node.patch
	0009-drm-panel-simple-Add-Hannstar-TQTM070CB501.patch
	0010-ARM-dts-sun6i-Add-GoClever-Orion-70L-tablet.patch
	0011-drm-panel-simple-Add-Hannstar-HSD070IDW1-A.patch
	0012-ARM-dts-sun6i-Add-Lark-FreeMe-70.2S-tablet.patch
	0013-sunxi-mmc-h6-fix.patch
	"
subpackages="$pkgname-dev"
builddir="$srcdir/linux-$_tag"

case "$CARCH" in
	aarch64*) _carch="arm64" ;;
	arm*) _carch="arm" ;;
esac


prepare() {
	default_prepare

	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make -j1 modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}

sha512sums="
0ec6b36a91384c1417f54f84678faf2660aed9132792e6d3803d93b2002e6696ce376e324ed8cfe47cfcfce3a8a4875b743b420fd92a2c5432ece4882037c4f7  linux-postmarketos-allwinner-orange-pi-5.13-20210707-1456.tar.gz
086c06ccb7d9491bc6da8ebd78e1bd8a57e14b34a3574446fa3a866eed42de574d46d7cb837c3941ec5fa161a4bc07fcf84fd55a6c0a3fbbda6d3779aad23087  config-postmarketos-allwinner.aarch64
2e3dd3e69575a456ad994dab7ea4e9405792aa522f164c660b09d1918673a3f9f81f28c751a86f1dd2712f98bc65f92577082c8aecd86b635a305cf87ead58a3  config-postmarketos-allwinner.armv7
126e0a65e04f22f14eac1281a69000d9d5b107ed8fd1b52f37e812751f55e6c45b0240ceac61c9d95ae7f0543aaf9d96b85a8532baf59283c077b9945e615367  0001-dts-add-dontbeevil-pinephone-devkit.patch
1dc710f5abacdd5698169d5e985efeca8114986c774c3d65e89812d08ceb3db5ebdc6dd64dd76a262c761f3bfc3ffb6bc237fd5236bd1f89c5975d12e42eda04  0002-dts-add-pinetab-dev-old-display-panel.patch
96849f40f6defc4ad7646256814bea5ac353c4e0479fd069365ff27456d63da801541ec616f6db736d60f3134384321a9eeb5bfbd555363bf1259c8de63d17fa  0003-dts-pinetab-add-missing-bma223-ohci1.patch
925bd630f3694d118b24f2f5a838ba105e4ca9ed8dd4294b6632310d8d6b1f421cdd673e2013758f9de7159050aa54e0f76e04edcff65f755842b1c95b6637e7  0004-arm64-dts-allwinner-Add-bluetooth-node-to-the-PineTa.patch
9bc6c2afadae8d367471f901b86a94be7fa70daf7a4bdde5bc6d4b1458b5ddd0e063a7765d962c68231c8f022d1c660d422124c31a2c899de768985d91d377ea  0005-dts-pinetab-make-audio-routing-consistent-with-pinep.patch
f6b35e61e88d933b612ee3212d71bb3e1aba8590c4985d9c17817426283df578476c00f3297683e43b570599384e78bb1fe76c506b3df93428e2101c4ccb1f3d  0006-dts-pinephone-remove-bt-firmware-suffix.patch
5e0566074fcb18b95c8ea079178f5d6716eb1ea325a25176e67f80eff1e5b10f90e282c8059a0a3315606546c61f05bd9ebad06adb67086cd123f0276cccbdc8  0007-media-ov5640-Implement-autofocus.patch
62809ac84db9d684b02d56dfc543451a1a13f319287466cfe913a847b945584ec60f7f556b1259ad3e26a44259a3fe587eeabeef6333f73a77204c3d3573f2ab  0008-dts-pinephone-drop-modem-power-node.patch
c61d4f86eddecca99df1ea1323f988e575f9c49130b64a0c50d61028ff96e24aaa3131fb3731e62e9b07db67019104d98933e31457891a5ce1e6010e06232875  0009-drm-panel-simple-Add-Hannstar-TQTM070CB501.patch
7f57e4ebc0b0cebb624f340c568f22e93116f2753b634d9bc75eea1a1bc013b8f9375e1efd24a20bff79c3725166df210a3a30bb7643a4525e3d40b63b91c536  0010-ARM-dts-sun6i-Add-GoClever-Orion-70L-tablet.patch
71fad7ecff66a2cb692f3dd3d02301c4b419873f27693de81f50226b8ab93c229299e7dc663fdd07a7e8f03a6541527f3bb449626f865fcfde6a780ef1b91976  0011-drm-panel-simple-Add-Hannstar-HSD070IDW1-A.patch
902708a5a8dd8a815d3d1793933d307c551e3c10368d1805431b70691ee5d4438b7146c466549f457468e365f94073a35ae3304928af7581e5dc91ba5c8fa682  0012-ARM-dts-sun6i-Add-Lark-FreeMe-70.2S-tablet.patch
2aec51b4cbe6349193c5db705b6788282844623930135be26dc62abf1a56c33e42538fde65c43cf0fb23677a71c9a929220baf3ba2ec89b2c485ffd9a6b1683f  0013-sunxi-mmc-h6-fix.patch
"
