# Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=soc-qcom-msm8916
pkgdesc="Common package for Qualcomm MSM8916 devices"
pkgver=11
pkgrel=0
url="https://postmarketos.org"
license="BSD-3-Clause"
arch="aarch64 armv7"
options="!check !archcheck !tracedeps pmb:cross-native"
depends="mesa-dri-gallium $pkgname-ucm swclock-offset"
subpackages="$pkgname-ucm $pkgname-modem"

_ucm_commit="4a2a2001bbaa99316e46bae192cd410e988e5b09"
source="$pkgname-$_ucm_commit.tar.gz::https://github.com/msm8916-mainline/alsa-ucm-conf/archive/$_ucm_commit.tar.gz
	q6voiced.conf
	"

package() {
	# parent package is empty
	mkdir -p "$pkgdir"
}

# Upstream keeps making breaking changes to UCM in patch releases.
# My last upstreaming efforts failed without ever getting a reply,
# since then UCM was entirely reworked like 3 times already...
# I don't want to fix this up every few months, so let's package a stable
# version for now. Once all the UCM refactoring upstream has settled down a bit
# we can investigate how to integrate it properly for upstreaming.
ucm() {
	provides="alsa-ucm-conf"

	cd "$srcdir/alsa-ucm-conf-$_ucm_commit"
	mkdir -p "$subpkgdir"/usr/share/alsa
	cp -r ucm2 "$subpkgdir"/usr/share/alsa
}

modem() {
	depends="msm-modem-rpmsg msm-modem-uim-selection q6voiced"
	install="$subpkgname.post-install"

	install -Dm644 q6voiced.conf "$subpkgdir"/etc/conf.d/q6voiced
}

sha512sums="
0fd13d74711e4f781cbf48907831e9b9d52cbfd9d3f2258120e34b71ac0627e1b8513740f19ec916bee687a267e20d54b7e6b1fd09d48efea8f2cb23622d0b8c  soc-qcom-msm8916-4a2a2001bbaa99316e46bae192cd410e988e5b09.tar.gz
3a4a9322839d4b3ef9d79668a37840a9f444954759ae3c512e694051d2f9a2573db42ad6c4c1a5c75eeb861232a27ba1a8cef9b503decd54ead25a96e3dd6f98  q6voiced.conf
"
